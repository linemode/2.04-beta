/*Tue Apr 27 10:38:58 1993 UTC (25 years, 9 months ago) by timbl*/
/*  */

/*              Initialisation module                   HTInit.h
**
**      This module resisters all the plug&play software modules which
**      will be used in the program.  This is for a browser.
**
**      To override this, just copy it and link in your version
**      befoe you link with the library.
*/

#include "HTUtils.h"

extern void HTFormatInit NOPARAMS;
extern void HTFileInit NOPARAMS;
/*

    */

