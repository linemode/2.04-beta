/*Tue Apr 27 10:38:45 1993 UTC (25 years, 9 months ago) by timbl*/
/*  */

/*                      GOPHER ACCESS                           HTGopher.h
**                      =============
**
** History:
**       8 Jan 92       Adapted from HTTP TBL
*/

#ifndef HTGOPHER_H
#define HTGOPHER_H

#include "HTAccess.h"
#include "HTAnchor.h"

/* extern int HTLoadGopher PARAMS((const char *arg,
        HTParentAnchor * anAnchor,
        int diag));
*/
extern HTProtocol HTGopher;

#endif /* HTGOPHER_H */
/*

    */

