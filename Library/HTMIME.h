/*Tue Apr 27 10:39:10 1993 UTC (25 years, 9 months ago) by timbl*/
/*  */

/*              MIME Parser                     HTMIME.h
**              -----------
**
**   The MIME parser stream presents a MIME document.
**
**
*/

#ifndef HTMIME_H
#define HTMIME_H

#include "HTStream.h"
#include "HTAnchor.h"

extern HTStream * HTMIMEConvert PARAMS((HTPresentation * pres,
                                        HTParentAnchor * anchor,
                                        HTStream * sink));


#endif




/*

    */

