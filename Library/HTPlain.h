/*Wed Apr 28 17:37:46 1993 UTC (25 years, 8 months ago) by timbl*/
/*  */

/*              Plain text object                       HTPlain.h
**              -----------------
**
**
*/

#ifndef HTPLAIN_H
#define HTPLAIN_H

#include "HTStream.h"
#include "HTAnchor.h"

extern HTStream* HTPlainPresent PARAMS((
        HTPresentation *        pres,
        HTParentAnchor *        anchor,
        HTStream *              sink));


#endif
/*

    */

