#!/bin/sh
echo "This script taken from the compilation of Line Mode 2.11"
mkdir -p /tmp/LineMode/unix
mkdir -p /tmp/Library/unix
cc -c -o /tmp/LineMode/unix/HTBrowse.o -I./Library/ -I./LineMode/ -DDEBUG -I./Library/ -I./LineMode/ -DVL=\"2.04-beta\" ./LineMode/HTBrowse.c
cc -c -o /tmp/LineMode/unix/GridText.o -I./Library/ -I./LineMode/ -DDEBUG -I./Library/ -I./LineMode/ ./LineMode/GridText.c
cc -c -o /tmp/LineMode/unix/DefaultStyles.o -I./Library/ -I./LineMode/ -DDEBUG -I./Library/ -I./LineMode/ ./LineMode/DefaultStyles.c
cc -c -o /tmp/Library/unix/HTParse.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTParse.c
cc -c -o /tmp/Library/unix/HTAccess.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTAccess.c
cc -c -o /tmp/Library/unix/HTTP.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTTP.c
cc -c -o /tmp/Library/unix/HTFile.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTFile.c
cc -c -o /tmp/Library/unix/HTBTree.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTBTree.c
cc -c -o /tmp/Library/unix/HTFTP.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTFTP.c
cc -c -o /tmp/Library/unix/HTTCP.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTTCP.c
cc -c -o /tmp/Library/unix/SGML.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/SGML.c
cc -c -o /tmp/Library/unix/HTML.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTML.c
cc -c -o /tmp/Library/unix/HTMLDTD.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTMLDTD.c
cc -c -o /tmp/Library/unix/HTChunk.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTChunk.c
cc -c -o /tmp/Library/unix/HTPlain.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTPlain.c
cc -c -o /tmp/Library/unix/HTWriter.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTWriter.c
cc -c -o /tmp/Library/unix/HTFWriter.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTFWriter.c
cc -c -o /tmp/Library/unix/HTMLGen.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTMLGen.c
cc -c -o /tmp/Library/unix/HTAtom.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTAtom.c
cc -c -o /tmp/Library/unix/HTAnchor.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTAnchor.c
cc -c -o /tmp/Library/unix/HTStyle.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTStyle.c
cc -c -o /tmp/Library/unix/HTList.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTList.c
cc -c -o /tmp/Library/unix/HTString.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK -DVC=\"2.04\" ./Library/HTString.c
cc -c -o /tmp/Library/unix/HTAlert.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK -DVC=\"2.04\" ./Library/HTAlert.c
cc -c -o /tmp/Library/unix/HTRules.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK -DVC=\"2.04\" ./Library/HTRules.c
cc -c -o /tmp/Library/unix/HTFormat.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTFormat.c
cc -c -o /tmp/Library/unix/HTInit.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTInit.c
cc -c -o /tmp/Library/unix/HTMIME.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTMIME.c
cc -c -o /tmp/Library/unix/HTHistory.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTHistory.c
cc -c -o /tmp/Library/unix/HTNews.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTNews.c
cc -c -o /tmp/Library/unix/HTGopher.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTGopher.c
cc -c -o /tmp/Library/unix/HTTelnet.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTTelnet.c
cc -c -o /tmp/Library/unix/HTWSRC.o -DDEBUG  -I./Library/ -DXMOSAIC_HACK ./Library/HTWSRC.c
ar r /tmp/Library/unix/libwww.a /tmp/Library/unix/HTParse.o /tmp/Library/unix/HTAccess.o /tmp/Library/unix/HTTP.o  /tmp/Library/unix/HTFile.o	/tmp/Library/unix/HTBTree.o /tmp/Library/unix/HTFTP.o /tmp/Library/unix/HTTCP.o  /tmp/Library/unix/SGML.o /tmp/Library/unix/HTML.o /tmp/Library/unix/HTMLDTD.o /tmp/Library/unix/HTChunk.o  /tmp/Library/unix/HTPlain.o /tmp/Library/unix/HTWriter.o /tmp/Library/unix/HTFWriter.o  /tmp/Library/unix/HTMLGen.o  /tmp/Library/unix/HTAtom.o /tmp/Library/unix/HTAnchor.o /tmp/Library/unix/HTStyle.o  /tmp/Library/unix/HTList.o /tmp/Library/unix/HTString.o /tmp/Library/unix/HTAlert.o  /tmp/Library/unix/HTRules.o /tmp/Library/unix/HTFormat.o /tmp/Library/unix/HTInit.o /tmp/Library/unix/HTMIME.o  /tmp/Library/unix/HTHistory.o /tmp/Library/unix/HTNews.o /tmp/Library/unix/HTGopher.o  /tmp/Library/unix/HTTelnet.o /tmp/Library/unix/HTWSRC.o 
ranlib /tmp/Library/unix/libwww.a
cc -o ./www  /tmp/LineMode/unix/HTBrowse.o /tmp/LineMode/unix/GridText.o /tmp/LineMode/unix/DefaultStyles.o -L/tmp/Library/unix -lwww
echo "The line mode browser should have been created as "www" in your current directory."
echo "If the build failed, you may want to remove/comment out line 30 in LineMode/HTBrowse.h and lines 57 and 58 in Library/HTTCP.c"
echo "Line Mode 2.04-beta works with http 1.0, it does not understand https (most websites) and it does not work with virtual hosted sites (almost all current websites) so what can be done with it is limited."
