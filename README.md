# 2.04-beta

1993 Public domain line mode browser from files and cvs repositories dating April/May 1993, when it became public domain. https://cds.cern.ch/record/1164399 https://dev.w3.org/cvsweb/libwww/LineMode/src/ https://www.w3.org/Daemon/old/Dist/

"If the build fails, you may want to remove/comment out line 30 in LineMode/HTBrowse.h and lines 57 and 58 in Library/HTTCP.c"